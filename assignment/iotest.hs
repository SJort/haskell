module IO where

import System.IO
import Data.Bits
import Data.Char

x = getLine

-- getLine return de input in een IO doos. met de >>= (bind) pak je dit uit de IO doos,
-- applied de functie UPPER op elke char met map, en bind zorg dan ook weer dat het 
-- weer in de IO doos wordt ingepakt met 'return' (ingebouwd)
-- je doet op de laatste regel alsnog return a, omdat a in de regel erboven geen doos is
-- bind heeft als criteria dat het een doos (met IO) returned
test = do
    a <- getLine >>= (\x -> return (map toUpper x))
    return a

--dit kan netter door het allemaal in de return te gooien,
--het gaat erom dat er een doos uitkomt

test2 = do
    a <- getLine -- nu ziet er gewoon een string in a
    return (map toUpper a) --uppercase de string en stop het in een doos


lees = do
    a <- readFile "message.txt"
    return a
