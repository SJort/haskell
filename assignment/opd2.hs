module Opd2 where

import           Data.List
import           Data.Ord

--1, 2
data Boek = Boek {
    titel :: String, -- titel als eerste omdat Ord ordend op eerste attribuut
    prijs :: Float,
    auteur :: String
} deriving (Show, Ord, Eq) -- Eq werkt out of the box


--3
b1 = Boek "D-titel" 2  "a-auteur"
b2 = Boek "D-titel" 2  "a-auteur"
b3 = Boek "B-titel" 3  "z-auteur"
b4 = Boek "E-titel" 4  "f-auteur"
b5 = Boek "A-titel" 5  "gf-auteur"
b6 = Boek "C-titel" 1  "d-auteur"
bs = [b1, b2, b3, b4, b5, b6]
--TEST
--b1 == b3 -> False
--b1 == b2 -> True
--sort bs -> A-titel eerste


--4
data Box a = Leeg | Iets a deriving Show


--5
packbook :: Boek -> Box Boek
packbook b = Iets b

packbooks :: [Boek] -> [Box Boek]
packbooks b = map packbook b
--TEST
--packbooks bs


unpackbook :: Box Boek -> Boek
unpackbook (Iets a) = a

unpackbooks :: [Box Boek] -> [Boek]
unpackbooks b = map unpackbook b
--TEST
--let x = packbooks bs
--unpackbooks x


 --6
data Sack a = Empty | Something a deriving Show --in engels omdat anders 'Iets' 2x is defined


--7
instance Functor Box where --met functor kun je een any functie loslaten op ingepakte waardes, waar je anders hulpfuncties voor moet schrijven
	fmap f Leeg = Leeg
	fmap f (Iets a) = Iets(f a)

instance Functor Sack where
	fmap f Empty = Empty
	fmap f (Something a) = Something(f a)

--TEST
--packbooks bs geeft hetzelfde als fmap packbook bs


--8
o81 = fmap Iets (fmap Something bs)
o82 = fmap Iets [1..5] --kun je willekeurige getallen generen?


--9
--recursive list = elk item in de list bestaat uit een node en een list
--gaat door totdat een list item bestaat uit een node en null als list

data List a = Niks | Ding a (List a) deriving Show

instance Functor List where
	fmap f Niks =Niks 
	fmap f (Ding a Niks) = Ding(f a) Niks
	fmap f (Ding a (Ding b z)) = Ding(f a) $ fmap f (Ding b z)

gl = Ding 1 (Ding 2 (Ding 3 Niks))

x = foldr (Ding) Niks o82
-- je begint met Niks, dat stop je in een Ding, en dat Ding stop je dan weer in een ander Ding
-- met het volgende getal uit de lijst

y = fmap (\(Iets g) -> Something g) x
-- de anonieme functie pakt een Iets met g erin en stop die g in een Something.
-- fmap past deze functie toe op elke waarde in onze zelfgemaakte List



