module Encrypt where
import           Data.Bits
import           Data.Char
import           Data.Function
import           System.IO
import           System.Random

randstr :: Int -> IO String
randstr x = sequence $ replicate x $ randomRIO ('A', 'Z')

--zelfde voor encrypt en decrypt
vernam :: String -> String -> String
vernam = zipWith (fmap chr . xor) `on` map ord

encrypt = do
    putStr "Enter file to encrypt:   "
    fname <- getLine
    file <- readFile fname
    key <- randstr (length file)
    let encrypted = vernam key file
    putStr "Encryption result:   "
    putStrLn encrypted
    putStr "Private key:   "
    putStrLn key
    writeFile "encrypted.txt" encrypted
    writeFile "private.key" key

decrypt = do
    file <- readFile "encrypted.txt" 
    key <- readFile "private.key"
    putStr "Private key:   "
    putStrLn key
    let decrypted = vernam key file
    writeFile "decrypted.txt" decrypted 
    putStr "Decryption result:   "
    putStrLn decrypted
