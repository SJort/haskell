module Test where

data Kleur = Rood|Blauw|Geel deriving Show
type Lengte = Float
type Breedte = Float
type Straal = Float
data Geofig = Vierkant Kleur Lengte|Rechthoek Kleur Lengte Breedte|Driehoek Kleur Lengte|Circel Kleur Straal deriving Show

getcolor :: Geofig -> Kleur
getcolor = t
