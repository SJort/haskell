module Encrypt where
import           Data.Bits
import           Data.Char
import           Data.Function
import           System.IO
import           System.Random

randstr :: Int -> IO String
randstr x = sequence ( replicate x ( randomRIO ('a', 'z')))

-- ord: char -> int
-- chr: int -> char
--vernam is hetzelfde voor encrypt en decrypt

strToInt :: String -> [Int]
strToInt a = fmap (ord) a

intToStr :: [Int] -> String
intToStr a = fmap (chr) a

--guidos vernam versie
gcrypt :: String -> String -> String
gcrypt a b = intToStr (encryptt (strToInt a) (strToInt b))

encryptt :: [Int] -> [Int] -> [Int]
encryptt [] [] = []
encryptt (a:b) (c:d) = (a `xor` c):(encryptt b d)

--jorts vernam versie
jcrypt :: String -> String -> String
jcrypt a b = intToStr(zipWith xor (strToInt a) (strToInt b))


encrypt = do
    putStr "Enter file to encrypt:   "
    fname <- getLine
    file <- readFile fname
    key <- randstr (length file)
    let encrypted = gcrypt key file
    putStr "Encryption result:   "
    putStrLn encrypted
    putStr "Private key:   "
    putStrLn key
    writeFile "encrypted.txt" encrypted
    writeFile "private.key" key

decrypt = do
    file <- readFile "encrypted.txt" 
    key <- readFile "private.key"
    putStr "Private key:   "
    putStrLn key
    let decrypted = gcrypt key file
    writeFile "decrypted.txt" decrypted 
    putStr "Decryption result:   "
    putStrLn decrypted
