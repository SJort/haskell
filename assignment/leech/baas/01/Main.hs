module Assignment1 where
import Data.Char

-- 1
data Color = Red | Blue | Yellow deriving (Show, Eq)

data Geofig =
              Square {
                  side   :: Double,
                  color  :: Color
              }|
              Rectangle {
                  width  :: Double,
                  height :: Double,
                  color  :: Color
              }|
              Triangle {
                  side   :: Double,
                  color  :: Color
              }|
              Circle {
                  radius :: Double,
                  color  :: Color
              }
              deriving (Show, Eq)

issquare (Square _ _)         = True
issquare _                    = False
isrectangle (Rectangle _ _ _) = True
isrectangle _                 = False
istriangle (Triangle _ _)     = True
istriangle _                  = False
iscircle (Circle _ _)         = True
iscircle _                    = False

-- 2
square    = Square 2.0 Red
rectangle = Rectangle 5.0 4.5 Blue
triangle  = Triangle 2.9 Yellow
circle    = Circle 2.4 Red
list      = [circle, rectangle, square, triangle, circle, rectangle, square, triangle]

-- 3
surface :: Geofig -> Double
surface x
        | issquare x    = 2 * (side x)
        | isrectangle x = 2 * ((width x) + (height x))
        | istriangle x  = 3 * (side x)
        | iscircle x    = pi * (radius x)^2

-- 4
circum :: Geofig -> Double
circum x
       | issquare x    = 4 * (side x)
       | isrectangle x = (height x) * (width x)
       | istriangle x  = (1 / 4) * (side x)^2 * sqrt 3
       | iscircle x    = 2 * pi * (radius x)

-- 5
filtersquare :: [Geofig] -> [Geofig]
filtersquare x = filter issquare x

filterrectangle :: [Geofig] -> [Geofig]
filterrectangle x = filter isrectangle x

filtertriangle :: [Geofig] -> [Geofig]
filtertriangle x = filter istriangle x

filtercircle :: [Geofig] -> [Geofig]
filtercircle x = filter iscircle x

-- 6
filtergeofig :: String -> [Geofig] -> [Geofig]
filtergeofig _ [] = []
filtergeofig x y
             | s == "square"    = filtersquare y
             | s == "rectangle" = filterrectangle y
             | s == "triangle"  = filtertriangle y
             | s == "circle"    = filtercircle y
             | otherwise        = error "invalid type name"
             where s = map toLower x

-- 7
getcolor (Square _ c) = c
getcolor (Rectangle _ _ c) = c
getcolor (Triangle _ c) = c
getcolor (Circle _ c) = c

filtercolor :: Color -> [Geofig] -> [Geofig]
filtercolor _ []     = []
filtercolor c (x:xs) = if   getcolor x == c
                       then x:filtercolor c xs
                       else filtercolor c xs

-- 8
maxsurface :: [Geofig] -> Double
maxsurface [] = error "empty list"
maxsurface x  = maximum $ map surface x

maxcircum :: [Geofig] -> Double
maxcircum [] = error "empty list"
maxcircum x  = maximum $ map circum x

-- 9
append :: Geofig -> [Geofig] -> [Geofig]
append x [] = x:[]
append x y  = x:y

-- 10
circumpershape :: [Geofig] -> [Double]
circumpershape []     = []
circumpershape (x:xs) = ((surface x) / (sum $ map surface (x:xs))) * 100.0 : circumpershape xs
