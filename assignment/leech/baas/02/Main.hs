module Assignment2 where
import Data.List
import Data.Ord

-- 1, 2
data Book = Book {
        title  :: String,
        author :: String,
        price  :: Float
} deriving (Show, Eq, Ord)

-- 3
book1 = Book "Blink" "Malcolm Gladwell" 8.00
book2 = Book "Modern Man in Search of a Soul" "C.G. Jung" 14.95
book3 = Book "The Art of Loving" "Eric Fromm" 8.99
book4 = Book "Owning Your Own Shadow" "Robert A. Johnson" 15.99
book5 = Book "The Book" "Alan Watts" 8.99
list = [book1, book2, book3, book4, book5]

compare :: Book -> Book -> Bool
compare a b = a == b

sort :: [Book] -> [Book]
sort = sortBy $ comparing title

-- 4
data Box a = EmptyBox | FullBox a
             deriving Show

instance Functor Box where
         fmap f EmptyBox    = EmptyBox
         fmap f (FullBox x) = FullBox (f x)

-- 5
packbox :: [Book] -> Box [Book]
packbox [] = EmptyBox
packbox x = FullBox x

extractbox :: Box [Book] -> [Book]
extractbox (FullBox x) = x

-- 6, 7
data Sack a = EmptySack | FullSack a
             deriving Show

instance Functor Sack where
         fmap f EmptySack    = EmptySack
         fmap f (FullSack x) = FullSack (f x)

-- 8
booksinsacksinboxes = fmap FullBox (FullSack list)

numsinboxes = fmap FullBox [1..10]

-- 9
-- not sure if this is right :/
data List a = EmptyList | FullList a (List a)
              deriving (Show, Eq, Ord)

instance Functor List where
         fmap f EmptyList      = EmptyList
         fmap f (FullList h l) = FullList (f h) $ fmap f l

boxinlist = foldr (FullList) EmptyList numsinboxes

-- ik begrijp niet helemaal hoe ik de laatste vraag moet aanpakken
--listinsack = fmap FullSack boxinlist
--sackinlist = foldr (FullList) EmptyList listinsack
