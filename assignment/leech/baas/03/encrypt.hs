module Encrypt where
import Data.Bits
import Data.Char
import Data.Function
import System.IO
import System.Random

randstr :: Int -> IO String
randstr x = sequence $ replicate x $ randomRIO ('A', 'Z')

vernam :: String -> String -> String
vernam = zipWith (fmap chr . xor) `on` map ord

main = do
          f <-      readFile "input.txt"
          key <-    randstr (length f)
          let out = vernam key f
          writeFile "encrypted.txt" out
          writeFile "private.key" key
