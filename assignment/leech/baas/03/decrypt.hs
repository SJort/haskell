module Decrypt where
import Data.Bits
import Data.Char
import Data.Function
import System.IO
import System.Random

vernam :: String -> String -> String
vernam = zipWith (fmap chr . xor) `on` map ord

main = do
          f <-      readFile "encrypted.txt"
          key <-    readFile "private.key"
          let out = vernam key f
          writeFile "decrypted.txt" out
