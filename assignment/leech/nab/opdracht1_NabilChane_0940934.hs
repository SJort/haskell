--1a
faca :: Int->Int
faca 0 = 1
faca n = n*faca(n-1)

--1b
facb :: Int -> Int
facb n | n == 0 = 1
       | n > 0 = n * facb(n-1)
       | n < 0 = error "Marh error"

--2a
nulpuntena::Double->Double->Double->[Double]
nulpuntena a b c = [x1, x2]
  where
    d = b * b - 4 * a * c
    x1 = (-b + sqrt d) / (2 * a)
    x2 = (-b -  sqrt d) / (2 * a)

--2b
nulpuntenb::Double->Double->Double->[Double]
nulpuntenb a b c
  | d == 0 = [x1]
  | d < 0 = []
  | d > 0 = [x1, x2]
  where
    d = b * b - 4 * a * c
    x1 = (-b + sqrt d) / (2 * a)
    x2 = (-b -  sqrt d) / (2 * a)

--2c
worpa = [(a, b, c) | a <- [1..6], b <- [1..6], c <- [1..6], (a+b+c) `mod` 5 == 0]

--2d
worpb n
  | n > 0 = [(a, b, c) | a <- [1..6], b <- [1..6], c <- [1..6], (a+b+c) `mod` n == 0]
  | otherwise = error "Wrong number"
