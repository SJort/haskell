--Opdracht 2, Nabil Chane, 0940934

import qualified Data.Map as Map
import qualified Data.List as List
import Data.Char (toLower)
import Data.Maybe
import Data.Ord
import System.IO

encoding = do
   print "Enter file name to load the text"
   fromFile <- getLine
   print "Enter file name to store the data"
   toFile <- getLine
   contents <- readFile fromFile
   print contents
   let encodedWord = encode contents (==)
   print encodedWord
   writeFile toFile (show encodedWord)  

decoding = do
   print "Enter file name to load the data"
   fromFile <- getLine
   print "Enter file name to store the text"
   toFile <- getLine
   contents <- readFile fromFile
   print contents
   let encodedData = read contents :: (HTree (String, Int), String)
   let decodedWord = decode (snd encodedData) (fst encodedData)
   print decodedWord
   writeFile toFile (show decodedWord)  

--2a & 2b
data HTree a = 	Leaf a
			|	Node a (HTree a) (HTree a) deriving (Show, Read)
			
type FreqList = [(String, Int)]

instance Eq a => Eq (HTree a) where
	(==) a b	= case a of
		Node{} -> node a == node b && left a == left b
		Leaf{} -> node a == node b

nodeSymbol :: HTree (a, b) -> a
nodeSymbol t = case t of
	(Node n _ _) -> fst n
	(Leaf n)	 -> fst n 
	
nodeValue :: HTree (a, b) -> b
nodeValue t = case t of
	(Node n _ _) -> snd n
	(Leaf n)	 -> snd n
	
node ::HTree a -> a
node t = case t of
	(Node n _ _) -> n
	(Leaf n) 	 -> n

left :: HTree t -> HTree t
left t = case t of 
	(Node _ l _) -> l
	
right :: HTree t -> HTree t
right t = case t of
	(Node _ _ r) -> r
	
getFreqList :: String -> FreqList
getFreqList = map (\ x -> ([head x], length x)) . List.group . List.sort

smallest :: [(HTree(String, Int))] -> HTree(String, Int)
smallest = List.minimumBy (comparing nodeValue)

mergeLeastTwo :: [HTree (String, Int)] -> (HTree (String, Int) -> HTree (String, Int) -> Bool) -> [HTree (String, Int)]
mergeLeastTwo leafs attribute =
    let least = smallest leafs
        secondLeast = smallest $ filter (not . attribute least) leafs
        rest = filter (\ x -> not (attribute x least) && not (attribute x secondLeast)) leafs
    in Node ("", nodeValue least + nodeValue secondLeast) least secondLeast : rest

getHTree :: [HTree (String, Int)] -> (HTree (String, Int) -> HTree (String, Int) -> Bool) -> [HTree (String, Int)]
getHTree leafs attribute
	| length leafs > 1 	= getHTree (mergeLeastTwo leafs attribute) attribute
	| otherwise			= leafs

getSymbol :: String -> HTree (String, Int) -> Int -> (String, Int)
getSymbol encodedWord tree depth = case tree of
	(Leaf n)		-> (fst n, depth)
	(Node n l r)	-> getSymbol (tail encodedWord) (if head encodedWord == '0' then l else r) (depth + 1)
	
eitherOr :: String -> String -> String
eitherOr x y
	| last y == '~' = x
	| otherwise		= y
	
encodeInnerHTree :: HTree (String, Int) -> HTree (String, Int) -> String
encodeInnerHTree l (Node n left right) = eitherOr ('0' : encodeInnerHTree l left) ('1' : encodeInnerHTree l right)
encodeInnerHTree l l1
	| nodeSymbol l == nodeSymbol l1 = ""
	| otherwise = "~"
					
encode :: String -> (HTree (String, Int) -> HTree (String, Int) -> Bool) -> (HTree (String, Int), String)
encode str attribute
    | null str      = (Leaf ("", 1), "")
    | otherwise     = let       freqList = getFreqList str
                                leafs = map Leaf freqList
                                tree = head $ getHTree leafs attribute
                                codes = [(nodeSymbol leaf, encodeInnerHTree leaf tree) | leaf <- leafs]
                                word = List.intercalate "" $ map (\letter -> fromJust (lookup [letter] codes)) str
                      in (tree, word)

decode :: String -> HTree (String, Int) -> String
decode encodedWord tree
	| encodedWord == "" 	= encodedWord
	| otherwise				= symbol ++ decode (drop usedSymbolsNum encodedWord) tree
	where	symbolTuple 	= getSymbol encodedWord tree 0
		symbol = fst symbolTuple
		usedSymbolsNum = snd symbolTuple