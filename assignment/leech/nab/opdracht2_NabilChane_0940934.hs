import Data.Char

--1a
euclid :: Integer->Integer->Integer
euclid x y
 | x `mod` y == 0 = y
 | y `mod` x == 0 = x
 | x > y = euclid (x`mod`y) y
 | y > x = euclid (y`mod`x) x
 
--1b
egcd :: Integer -> Integer -> (Integer,Integer,Integer) 
egcd 0 b = (b, 0, 1) 
egcd a b =
 let (g, s, t) = egcd (b `mod` a) a
  in (g, t - (b `div` a) * s, s)

posmod :: Integer->Integer->Integer
posmod a b   
 | g == 1 = s `mod` b
 where (g, s, _) = egcd a b
 
--2
coprime :: Integer -> Integer -> Bool
coprime 0 0 = error "(0,0) kan niet"
coprime a b = (euclid a b) == 1

modulus :: Integer->Integer->Integer
modulus p q = p*q

totient :: Integer->Integer->Integer
totient p q = (p-1) * (q-1)

--3a
rsaencrypt::(Integer,Integer)->Integer->Integer
rsaencrypt (e,m) x = (x^e)`mod`m

--3b
rsadecrypt::(Integer,Integer)->Integer->Integer 
rsadecrypt (d,m) x = (x^d)`mod`m

--4
send::(Int,Int)->Char->Int
send (e,m) x = (c^e)`mod`m
 where c = ord x
 
receive::(Int,Int)->Int->Char
receive (d,m) x = chr c
 where c = (x^d)`mod`m