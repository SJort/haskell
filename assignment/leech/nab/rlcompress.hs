--Opdracht 1, Nabil Chane, 0940934

import System.IO  
import Control.Monad
import qualified Data.List as List
import qualified Data.List.Split as Split
import qualified Data.Char as Char

encoding = do
   print "Enter file name to load the text"
   fromFile <- getLine
   print "Enter file name to store the data"
   toFile <- getLine
   contents <- readFile fromFile
   print contents
   let encodedWord = runLengthEncode contents
   print encodedWord
   writeFile toFile encodedWord
   print (show . length $ contents)
   print (show . length $ encodedWord)
   
decoding = do
   print "Enter file name to load the data"
   fromFile <- getLine
   print "Enter file name to store the text"
   toFile <- getLine
   contents <- readFile fromFile
   print contents
   let decodedWord = runLengthDecode contents
   print decodedWord
   writeFile toFile decodedWord
   print (show . length $ contents)
   print (show . length $ decodedWord)

--1a
runLengthEncode :: String -> String
runLengthEncode = concat . map (compressRun) . List.group

compressRun :: String -> String
compressRun xs 
	| length xs == 1 = xs
	| otherwise      = (show . length $ xs) ++ [head xs]

--1b
runLengthDecode :: String -> String
runLengthDecode xs = concat $ map (decomporessRun) compressedRuns
  where 
    compressedRuns = 
      Split.split 
        (Split.dropFinalBlank . Split.keepDelimsR $ Split.whenElt Char.isLetter)
        xs

decomporessRun :: String -> String
decomporessRun compressedRun
	| length compressedRun == 1 = compressedRun
	| otherwise = replicate (read $ init compressedRun) (last compressedRun)