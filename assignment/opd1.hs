module Opd1 where

-- 1
data Kleur = Rood|Blauw|Geel deriving Show

instance Eq Kleur where
	Rood == Rood = True
	Blauw == Blauw = True
	Rood == Geel = True
	_ == _ = False

type Lengte = Float
type Breedte = Float
type Straal = Float
data Geofig = Vierkant Kleur Lengte|Rechthoek Kleur Lengte Breedte|Driehoek Kleur Lengte|Circel Kleur Straal deriving Show


-- 2
x1 = Vierkant Rood 2.0
x2 = Rechthoek Blauw 4.0 3.0
x3 = Driehoek Geel 5.0
x4 = Circel Rood 6.0
x5 = Circel Geel 1.0
x6 = [x1, x2, x3, x4, x5]


-- 3
oppervlakte :: Geofig -> Float
oppervlakte (Vierkant _ l) = l * l
oppervlakte (Rechthoek _ l b) = l * b
oppervlakte (Driehoek _ l) = 0.5 * l * l
oppervlakte (Circel _ r) = pi * r * r

-- 4
omtrek :: Geofig -> Float
omtrek (Vierkant _ l) = 4 * l
omtrek (Rechthoek _ l b) = 2 * l * b
omtrek (Driehoek _ l) = 3 * l
omtrek (Circel _ r) = 2  * pi * r

-- 5
-- stack:::
-- returnvierkant :: [Geofig]->[Geofig]
-- returnvierkant list= [ x | x@(Vierkant _ _) <-  list]

getshape :: Geofig -> String
getshape (Vierkant _ _) = "v"
getshape (Rechthoek _ _ _) = "r"
getshape (Driehoek _ _) = "d"
getshape (Circel _ _) = "c"

getvierkant [] = []
getvierkant (h:t) = if getshape h == "v" then h:getvierkant t else getvierkant t

getrechthoek [] = []
getrechthoek (h:t) = if getshape h == "r" then h:getrechthoek t else getrechthoek t

getdriehoek [] = []
getdriehoek (h:t) = if getshape h == "d" then h:getdriehoek t else getdriehoek t

getcircel [] = []
getcircel (h:t) = if getshape h == "c" then h:getcircel t else getcircel t


--6
getform :: String -> [Geofig] -> [Geofig]
getform s l 
	| s == "Vierkant" = getvierkant l
	| s == "Rechthoek" = getrechthoek l
	| s == "Driehoek" = getdriehoek l
	| s == "Circel" = getcircel l
	| otherwise = []

--7
getcolorvierkant :: Geofig -> Kleur
getcolorvierkant (Vierkant k _) = k

getcolorrechthoek :: Geofig -> Kleur
getcolorrechthoek (Rechthoek k _ _) = k

getcolordriehoek :: Geofig -> Kleur
getcolordriehoek (Driehoek k _) = k

getcolorcircle :: Geofig -> Kleur
getcolorcircle (Circel k _) = k

getcolor :: Geofig -> Kleur
getcolor s
    |getshape s == "v" = getcolorvierkant s
    |getshape s == "r" = getcolorrechthoek s
    |getshape s == "d" = getcolordriehoek s
    |getshape s == "c" = getcolorcircle s
    |otherwise = Geel

getcolors :: [Geofig] -> Kleur -> [Geofig]
getcolors [] k = []
getcolors (h:t) k = if getcolor h == k then h:getcolors t k else getcolors t k

--8
findbigg :: Geofig -> [Geofig] -> Geofig
findbigg g [] = g
findbigg g (h:t) = if omtrek h > omtrek g then findbigg h t else findbigg g t

findbiggestomtrek :: [Geofig] -> Geofig
findbiggestomtrek (h:t) = findbigg h t

findbiggopp :: Geofig -> [Geofig] -> Geofig
findbiggopp g [] = g
findbiggopp g (h:t) = if oppervlakte h > oppervlakte g then findbiggopp h t else findbiggopp g t

findbiggestopp :: [Geofig] -> Geofig
findbiggestopp (h:t) = findbiggopp h t


--9
addgeo :: [Geofig] -> Geofig -> [Geofig]
addgeo a b = a ++ [b]


--10
percentt :: Float -> Float -> Float
percentt a b = b / a * 100

totalopp :: [Geofig] -> Float -> Float
totalopp [] a = a
--totalopp (h:t) a = totalopp h (omtrek h) + a
totalopp (h:t) a = totalopp t a + oppervlakte h

totaloppervlakte :: [Geofig] -> Float
totaloppervlakte a = totalopp a 0

percenthelper :: [Geofig] -> [Float] -> Float -> [Float]
percenthelper [] a total = a
percenthelper (h:t) a total = percenthelper t (a ++ [percentt total (oppervlakte h)]) total

percent :: [Geofig] -> [Float]
percent [] = []
percent a = percenthelper a [] (totaloppervlakte a)





