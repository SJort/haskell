# Setup
Download Haskell compiler:
```shell
sudo pacman -S ghc
```
Haskell formatter:
```shell
sudo pacman -S stylish-haskell
```

# Editing
Create test file:
```shell
vim test.hs
```
Put the following in the file:
```haskell
module Test where

addd :: Int -> Int
addd x = x + 1
```
Load the file in compiler:
```shell
ghci
:l test.hs
```
Reload the last file:
```shell
:r
```
Test the file:
```shell
addd 1
```
Format the file in Vim:
```
:%!stylish-haskell
```








