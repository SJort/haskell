module OneA where
	
	--to execute:
	--open ghci
	--do -l fileName
	--type in the function with parameters

	add x y = x+y
	
	-- usage: multfive [3, 4]
	-- multiplies every element in the list with 5
	multfive::[Int]->[Int]
	multfive [] = []
	multfive (x:xs) = (x*5):multfive xs	-- () here is a pattern match of a list, : here is append

	--usage: multa 3 4
	-- multiplies given number with given number
	multa::Int->[Int]->[Int]
	multa _ [] = []		--wildcard _ , any input
	multa x (h:t) = (x*h):multa x t


	-- usage: apply (+) 6 [1..7]
	-- adds 6 to every element in the list
	apply::(Int->Int->Int)->Int->[Int]->[Int]	--() means take a function. here it means: take a function that takes two integer arguments and spits out 1 integer. for example the (+) function
	apply f x [] = []
	apply f x (head:tail) = (f x head):apply f x tail
	

	--first parameter: a function, second parameter: a list. generic apply
	genapply::(a->a)->[a]->[a]			--a can be anything, double to double for example. (a->a) is a function parameter which makes something from a?
	genapply _ [] = []				--stop condition for recursion
	genapply f (x:xs) = (f x):genapply f xs		-- apply function f on x, then apply f on the rest of the list. () is a match on a list here
	-- this function already exists in haskell because of its usefullness, its called map

	-- 1A
	faca::Int->Int
	faca 1 = 1
	faca i = i * faca (i-1) 	

	-- 1B	
	facb::Int->Int
	facb i
		| i == 1 = 1
		| i > 1 = i * facb (i-1)

	-- 2A
	-- Sample usage: 2 5 1
	nulpuntena::Double->Double->Double->[Double]
	nulpuntena a b c = [((-b+sqrt((b*b)-(4*a*c)))/(2*a)),((-b-sqrt((b*b)-(4*a*c)))/(2*a))]

	
	--2B
	nulpuntenb::Double->Double->Double->[Double]
	nulpuntenb a b c
		|d < 0 = []
		|d == 0 = [((-b)/(2*a))]
		|otherwise = [((-b+sqrt(d))/(2*a)),((-b-sqrt(d))/(2*a))]
		where d = (b*b) - (4*a*c)

	--2C
	gooi1::[(Int, Int, Int)]
	gooi1 = [(x ,y, z) | x <- [1..6], y <- [1..6], z <- [1..6], (x+y+z) `mod` 5 == 0]

	--2D
	gooi2::Int -> [(Int, Int, Int)]
	gooi2 n = [(x ,y, z) | x <- [1..6], y <- [1..6], z <- [1..6], (x+y+z) `mod` n == 0]
