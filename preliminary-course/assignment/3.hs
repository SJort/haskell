module Three where
	import Data.List

	--1A
	--
	--Function
	f::Double->Double
	f x = x*x

	differentieer::(Double->Double)->Double->Double->Double
	differentieer f p x = ((f(x+p)) - (f x)) / p 

	--1B
	--
	--Left Riemann Sum
	integreer::(Double->Double)->Double->Double->Double->Double
	integreer f a b p 
		|(a >= b) = 0.0
		|otherwise = (f a)*p + (integreer f (a+p) b p)


	--2
	--(Eq a) => only allows comparable types, it says something about the first [a]
	dubbelen::(Eq a) => [a]->[a]
	dubbelen [] = []
	--nub removes duplicates from list (keeps first occurence)
	--removes elements on the right from the list on the left: basically subtracts the list on the right from the left
	dubbelen a = nub (a \\ (nub a))

	--aaabbcddeeff
	--nub a -> abcdef
	--a - nub a -> aaabbcddeeff - abcdef -> aabdef
	--nub: aabdef -> abdef
	

	--3
	--all possible combinations
	
	--je moet een lijst terug geven met alle uitkomsten die voldoen
	--je wilt dus door de hele lijst van lijsten heen loopen:
	--als de huidige lijst voldoet, sla hem op, anders: skip hem: dit doet filter
	
	--straight: kijk naar de count 1'tjes van de convert result; deze moet mininaal 5 zijn
	testStraight :: [Integer] -> Bool
	testStraight [] = False
	testStraight numberList = ([1, 1, 1, 1, 1, 0] == cNumberList) || ([0, 1, 1, 1, 1, 1] == cNumberList)
		where cNumberList = (fst ((convert numberList)))


	--twopair: de count van twee dingen moet allebij 2 zijn
	--dus tel gewoon de count van 2'tjes uit de convert output. filter de list op 2'tjes
	testTwoPair::[Integer] -> Bool 
	--moker numberList = 2 == (length (filter twee (fst(convert numberList))))
	testTwoPair numberList = 2 == (length (filter (\x -> x > 1) (fst(convert numberList))))
	--je filterd de lijst op de 'hoger dan 1' result van convert, en kijkt vervolgens of de lengte van
	--het resultaat van de filter twee is, full houses worden dus meegerekend: 3 > 1 
	
	testFullHouse::[Integer] -> Bool
	testFullHouse numberList = (testTwoPair numberList) && (isAtLeastCount 3 numberList)

	--convert list: check for maximum, if bigger than 4, true
	--take the counts result of convert -> get the highest number -> compare that number
	--basically: does something occur at least x times in this list?
	isAtLeastCount::Integer -> [Integer] -> Bool
	isAtLeastCount _ [] = False
	isAtLeastCount number numberList = number <= maximum (fst (convert numberList))
	
	
	stenen = [[a,b,c,d,e]|a<-s,b<-s,c<-s,d<-s,e<-s]
	    where s = [1..6]

	--count occurences in list
	count :: Integer -> [Integer] -> Integer
	count c [] = 0
	count c (x:xs)
	    | c == x    = 1 + (count c xs)
	    | otherwise = count c xs


	--list to tuples with counts
	convert list = ([a,b,c,d,e,f], list) where
	    a = count 1 list
	    b = count 2 list
	    c = count 3 list
	    d = count 4 list
	    e = count 5 list
	    f = count 6 list

	testConvert = convert [1, 2, 3, 3, 4, 5]

	amountOfSames::Integer->[[Integer]]
	amountOfSames x = filter (isAtLeastCount x) stenen

	poker = amountOfSames 5
	fourOfAKind = amountOfSames 4
	threeOfAKind = amountOfSames 3
	pair = amountOfSames 2

	twoPair = filter testTwoPair stenen

	straight = filter testStraight stenen

	fullHouse = filter testFullHouse stenen

	lPoker = length (amountOfSames 5)
	lFourOfAKind = length (amountOfSames 4)
	lThreeOfAKind = length (amountOfSames 3)
	lPair = length (amountOfSames 2)
	lTwoPair = length (filter testTwoPair stenen)
	lStraight = length (filter testStraight stenen)
	lFullHouse = length (filter testFullHouse stenen)

	--a two of a kind could also be a four of a kind so number is negative
	lBust = length stenen - lPair - lStraight
	bust = (stenen \\ pair) \\ straight

	

	
