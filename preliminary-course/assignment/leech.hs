import Data.List

p = 1/10000

f x = x^3 + 2*x^2
dfdx x = 3*x^2 + 4*x

type Func = (Double -> Double)

-- Opdracht 1a
differentieer :: Func -> Double -> Double -> Double
differentieer f p x = (f(x + p) - f(x)) / p

-- Opdracht 1b
integreer:: Func -> Double -> Double -> Double -> Double
integreer f a b p
    | b > a = ((integreer f a (b - p) p) + ((f b) * p))
    | b <= a = (f b) * p
    
-- Opdracht 2
dubbelen :: [Char] -> [Char]
dubbelen s = nub $ s \\ filter
    where filter = ['A'..'z'] ++ ['0'..'9']
-- test: dubbelen "aabbbcdeeeef"
-- http://hackage.haskell.org/package/base-4.12.0.0/docs/Data-List.html#v:nub

-- Opdracht 3
-- Dobbelstenen
stenen = [[a,b,c,d,e]|a<-s,b<-s,c<-s,d<-s,e<-s]
    where s = [1..6]

-- Hoe vaak komt 'c' voor in de lijst
count :: Integer -> [Integer] -> Integer
count c [] = 0
count c (x:xs)
    | c == x    = 1 + (count c xs)
    | otherwise = count c xs

-- Van list naar count list
convert list = ([a,b,c,d,e,f], list) where
    a = count 1 list
    b = count 2 list
    c = count 3 list
    d = count 4 list
    e = count 5 list
    f = count 6 list

-- length (Int) to Integeral
integralLength = (fromIntegral.length)
    
-- Maximaal aantal mogelijke combinaties
maxCombinations = integralLength stenen

-- Aantal combinaties met x aantal dezelfde ogen
countDitto x = integralLength $ ditto x stenen

-- Lijst van combinaties in de lijst met x aantal dezelfde ogen
ditto :: Integer -> [[Integer]] -> [[Integer]]
ditto x list = filter (elem x) $ map fst $ map convert list

-- Hulp functies
getDuplicates = map (nub.(\\ [1..6]))

-- Kansen

-- 5 stenen met gelijke ogen
poker = countDitto 5 / maxCombinations

-- 4 stenen met gelijke ogen
fourOfAKind = countDitto 4 / maxCombinations

-- 3 stenen met gelijke ogen (zonder 2 dezelfde, want dat is een Full house)
threeOfAKind = combinations / maxCombinations
    where combinations = integralLength $ filter (notElem 2) $ ditto 3 stenen

-- 3 stenen met gelijke ogen en nog eens 2 stenen met gelijke ogen, bijv. 44554
fullHouse = combinations / maxCombinations
    where combinations = integralLength $ filter (elem 2) $ ditto 3 stenen
    
-- Twee paar met gelijke ogen, bijv. 35453
twoPair = combinations / maxCombinations
    where combinations = integralLength $ ditto 2 $ ditto 2 stenen
    
-- Een paar met gelijke ogen, bijv. 12344
onePair = combinations / maxCombinations
    where combinations = integralLength $ filter (notElem 2) $ getDuplicates $ filter (notElem 3) $ ditto 2 stenen
    
straight = combinations / maxCombinations
    where combinations = 2 * foldr (*) 1 [1..5]
    
bust = 1 - poker - fourOfAKind - threeOfAKind - fullHouse - twoPair - onePair - straight
