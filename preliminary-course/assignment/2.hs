module Two where

	import Data.Char


	--1A
	--euclid 5238 1242 should return 54
	euclid::Integer->Integer->Integer
	--keep looping until the rest is 0
	euclid x 0 = x
	euclid x y = euclid y (x `mod` y)
			

	--1B
	--given:
	egcd :: Integer -> Integer -> (Integer,Integer,Integer)
	egcd 0 b = (b, 0, 1)
	egcd a b =
		let (g, s, t) = egcd (b `mod` a) a
			in (g, t - (b `div` a) * s, s)	

	--function that takes the middle negative number
	mid(_,x,_) = x

	--add the modulo to the negative number if negative
	mijnegcd e m
		|d<0 = d+m   --plus m om een positief getal te krijgen
		|otherwise = d
		where d = mid $egcd e m

	--2A
	--test values:
	p = 7
	q = 13

	--modulos: m
	m = p*q
	--eulers totient function
	m' = (p-1)*(q-1)
	
	--hand choosen relative prime number(43)
	e = 5
	
	--voorgaande congruentie oplossing
	d = mijnegcd e m'

	--43 is nu prive, 67 is public en 72 is de modulos
	

	--3A
	--usage: rsacrypt (5, 29) 75 gives 17, rsacrypt (5, 91) 17 gives 75
	--5 and 29 are the keys, 91 is the modulos
	rsacrypt::(Integer, Integer)->Integer->Integer
	rsacrypt (e, m) x = (x^e) `mod` m

	--4
	charToInt::Char->Integer
	charToInt a = toInteger (ord a)

	intToChar::Integer->Char
	intToChar a = chr (fromInteger a)

	--5
	--gebruik alleen de public keys om te encrypten en de private keys om te decrypten	
