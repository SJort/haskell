module Notes where
	
	--to execute:
	--open ghci
	--do -l fileName
	--type in the function with parameters

	--code leest van links naar rechts
	--(+) 8 4	
	--eerst wordt anonieme functie een + functie
	--daarna een +8 functie
	--daarna een (+8)4
	--let a = add 8
	--a 4 geeft dan 12
	--let b = a 4
	--b geeft dan meteen antwoord
	add x y = x+y 


	--(Int->Int)->Int->Int
	--hier is de eerste parameter een functie die van een int een int maakt, de tweede een int, de return type ook een int
	--
	--
	--function apply: 
	--
	neg::Int->Int
	neg x
		|x < 0 = x
		|otherwise = -x

	--goal: apply function schrijven die neg doet op alles in een lijst
	
	--apply::(Int->Int)->[Int]->[Int]
	--apply f [] = []
	--apply f (x:xs) f x:apply f xs		
--haakjes geeft aan dat er iets in de lijst zit, (head, second, rest) mag bijv ook. dubbele punt knoopt shit aan elkaar. dus apply fuck op head, en plak daar de recursive rest aan


	-- kan algemener:
	apply::(a->b)->[a]->[b]
	apply f [] = []
	apply f (x:xs) =  f x:apply f xs
