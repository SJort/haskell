module Two where
	--endless printing
	restklasse::Int->Int->[Int]
	restklasse start modulos = [start + (n*modulos)|n<-[1..]]
	--take 10 $restklasse 1 5
	
	restklasse2 start modulos = [start, start+modulos..]

	--takeWhile (>0) $restklasse2 1 10      goes until condition is met
	

	--restklasse2 heeft geen defenitie
	restklasse3::Int->Int->[Int]
	restklasse3 start modulos = [start, start+modulos..]
	--nu wel, anders werdt er vanzelf een enum type gebruikt wat vanzelf meer geheugen alloceerd bij overflows
	--deze stopt wel vanzelf, omdat er een patroon wordt gedefineerd met .. en deze afgekapt wordt bij een overflow omdat het patroon dan niet meer gevolgd wordt	


	egcd :: Integer -> Integer -> (Integer,Integer,Integer)
	egcd 0 b = (b, 0, 1)
	egcd a b =
		let (g, s, t) = egcd (b `mod` a) a
			in (g, t - (b `div` a) * s, s)	

	--wat kun je met egcd: egcd 3 10 geeft bijvoorbeeld -3
	--e=3 d=7 en m=10  geeft -3. geeft e en m mee, krijg d terug
	--we hebben dus intresse in het middelste getal van egcd
	--functie die middelste getal pakt:
	mid(_,x,_) = x

	--nu kunnen we: mid $egcd 3 10     geeft -3 
	
	--mijnegcd e m = let d = mid $ egcd e m

	mijnegcd e m
		|d<0 = d+m   --plus m om een positief getal te krijgen
		|otherwise = d
		where d = mid $egcd e m

	--HOPPA DIT WAS AL EEN OPDRACHT
	
	--lol dit is eigenlijk een functie die altijd een getal teruggeeft
	p = 7
	q = 13

	m = p*q
	m' = (p-1)*(q-1)
	
	e= 43
	
	d = mijnegcd e m'

	--43 is nu prive, 67 is public en 72 is de modulos

