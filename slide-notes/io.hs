module Main where
	import Data.Char
	import System.IO

main = do
	inhoud <- readFile "student.txt"
	let woorden = words inhoud
	putStrLn inhoud
	return woorden
