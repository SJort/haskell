module Main where

	import Data.Char
	import System.IO

	main = do
		putStr "Hello World..."

	----main function needed in Main to run independend of ghci
	

	--de return met de haakjes maakt er een monadische functie van
	kwadraat x = return (x*x)

	wortel x = return (sqrt x)

	data Doos a = Leeg
		|Doos a deriving Show

	--instance Monad Doos where
	--	return a = Doos a
	--	(Doos a) >>= f = f a
	--	Leeg >>= f = Leeg

--applicative: haalt iets uit een container en applied de functie erop <*>
--pakt data ingepakt in een datatype afgeleid van functor: Maybe, Doos: en applied the given functie etop
--fmap (+5) (Just 6)  apply de functie +5 op de container Just met daarin 6
--
--
--monad: een typeclass(interface)
--then: >> :
--return
--bind: >>=
--fail
--alleen return en bind moet je zelf defineren
--
--return: pakt iets in in een monarisch datatype
--
--bind(infix operator): pakt aan a ingepakt in een monad en een functie die een kale waarde inpakt
--dus: hij trekt de waarde uit de container en applied de functie erop, returns de uitkomst ervan
--het lijkt erg op fmap maar niet hetzelfde
--het lijkt erg op pipelines als in linux
--
--then: 
--
--do: <- pak het uit de container, de rechterkant van het pijltje is altijd ingepakt
--
--
--IO: datatype: alles uit de buitenwereld word ingepakt in het IO datatype
--je mag het niet uitpakken
--
--
--compile shit:
--ghc -o demo demo.hs
--run: ./demo
