module Test where

--typeclass: basically een interface waarmee je ook operators als == kan overriden

import Data.Char
import Control.Applicative

data Doos a = Leeg
	| Doos a deriving Show


kwadraat::Int->Int
kwadraat x = x*x

--tik de verschillende verschijnselen weg met pattern matching
--een lege doos
dooskwadraat Leeg = Leeg
--doos met een a erin
dooskwadraat (Doos a) = Doos(a*a)
--dooskwadraat (Doos 5)
--nadeel: ook andere dingen dan Int kan erin
--algemener aanpakken:
functieindoos::(a->b) -> (Doos a) -> (Doos b)
functieindoos f Leeg = Leeg
functieindoos f (Doos a) = Doos(f a)
--usage: functieindoos (+6) (Doos 7)
--functieindoos (++"Wessel") (Doos "Oele")
--nu kunnen we dus doen: functieindoos kwadraat (Doos 4)


--Functor typeclass(interface)
--dit is basically wat we net gebouwd hebben
instance Functor Doos where
	fmap f Leeg = Leeg
	--apply f op de waarde a in de Doos
	fmap f (Doos a) = Doos(f a)
--nu kun je: fmap kwadraat (Doos 7)
--het past een functie toe op ingepakte waarden
--
--als je hebt Just "hallo"
--je kunt fmap ("Doei"++ (Just "hallo")
--dit kan normaal niet omdat "hallo" is ingepakt met Maybr


--pakt iets in in het betreffende datatype
instance Applicative Doos where
	pure x = Doos x
--usage: pure 5 :: (Doos Int)
--dit betekent: doe pure 5 niet op een int maar op een Doos Int
--
--applicative functor: <*>
--die applied een Doos met een functie erin los op een andere doos en returned de values in een Doos
--staat allemaal wel duidelijk in slides vgm
--synonym for fmap: <$>, = infix, hiermee kun je de haakjes weghalen
--
--al deze fmap functies en alles werken dus met Dozen als het ware
--dit wordt gebruikt voor IO
--je haalt het tijdelijk uit de verpakking: kun je veilig er operaties mee doen, en vervolgens doe je het weer terug
--je schermt dus de IO af van het functionele gedeelte van het programma
