module Les1 where

import Data.Char
import Data.List

--TYPESYNONIEMEN
type Voornaam = String
type Achternaam = String
type Leeftijd = Integer

type Persoon = (Voornaam,Achternaam,Leeftijd)

jarig :: Persoon -> Persoon
jarig (v,a,l) = (v,a,l+1)

--aanroepen: jarig("jort","jort2", 3)
--geeft: jarig("jort","jort2", 4)

type Bewerking = (Int -> Int)

laatlos :: Bewerking -> Int -> Int
laatlos b x = b x

--aanroepen: laatlos (*8) 8 geeft 64

--eigen datatype defineren :
--altijd hoofdletter
--
--manier 1:
data Stoplicht = Rood|Oranje|Groen deriving Show
--let x = Rood
--t x -> Datatype Stoplicht
--
--zelfgeschreven datatypes hebben geen toString methods etc
--zet deriving Show erachter om Haskell vanzelf een toString te laten uitvogelen
--nu kun je x zeggen en Haskell print Rood
--


