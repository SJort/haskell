module Test where


data Richting = North|West|South|East|True deriving Show


data Lijst a= Leeg
	| Hoofd a (Lijst a) deriving Show


--dit allemaal ook slides:
push::Lijst a -> a -> Lijst a
--element pushen op lege lijst
push Leeg x = Hoofd x Leeg
--x pushen op lijst met tenminste een kop
push (Hoofd a rest) x = Hoofd x (Hoofd x rest)


data Tree a = Niks
	--elke node heeft twee child nodes
	--Node "hallo" Niks Niks
	| Node a (Tree a) (Tree a) deriving Show

--alles wat onderling vergelijkbaar is is afgeleid van de typeclass Eq
--5 == 5 True
--Ord typeclass: typen die op volgorde geplaatst kunnen worden
--5 < 7 True, 'a' > 'b' False
--Show: voor alles wat op het scherm afgedrukt kan worden

type Prijs = Int
type Adres = String

data Huis = Huis Prijs Adres deriving Show 

instance Eq Huis where
	--integers behoren al tot de Eq class dus a == b mag gewoon
	--huizen zijn nu alleen gelijk als adres gelijk is
	Huis _ a == Huis _ b = a == b
	--alle andere gevallen wijzen we af
	_ /= _ = False

--instance Ord Huis where
--	(Huis a) <= (Huis b) = a <= b

x = Huis 30 "a"
y = Huis 40 "b"
z = Huis 30 "a"
--x==y False
--x==z True
