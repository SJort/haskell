module Test where

telop x y = x+y

data Stoplicht = Rood|Oranje|Groen
	deriving Show

--je weet hier niet welke parameter wat is
data Geschrift0 = String String Int
	deriving Show

--overal waar een titel staat is achter de schermen een string
type Titel = String
type Auteur = String
type Prijs = Int
type Uitgever = String

--code is nu leesbaarder
data Geschrift = Boek Titel Auteur Prijs
	| Weekblad Titel Uitgever
	deriving Show
--een object van type geschrift kan either een Boek of een Weekblad zijn -> polymorphise

--let x = Boek "De lel" "Jort" 654
--let y = Weekblad "khonker" "lmao"

--krijg titel van boek:
titel (Boek t a p) = t
auteur (Boek _ a _) = a --wildcards puur voor leesbaarheid
--accessor function / getters

--accessor functions gratis:
--data Lmao = Boek2
--	{
--		Titel2 :: String,
--		Auteur2 :: String,
--		Prijs2 :: Int
--	}
--	deriving Show

--je kunt Data.maybe gebruiken om data te controleren of het legit is
--zoals bij een wortel proberen te nemen van een negatief getal
--moet je wel in die wortelfunctie eerst iets met maybe doen
--like if (maybe(-1) == Nothing) return 0
